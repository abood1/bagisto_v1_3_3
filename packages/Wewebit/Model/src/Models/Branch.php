<?php

namespace Wewebit\Model\Models;

use Illuminate\Database\Eloquent\Model;
use Wewebit\Model\Contracts\Branch as BranchContract;

class Branch extends Model implements BranchContract
{
    protected $fillable = [
        'lat',
        'lng',
        'description',
        'name',
    ];
}