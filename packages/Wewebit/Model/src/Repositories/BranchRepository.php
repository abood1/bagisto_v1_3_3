<?php

namespace Wewebit\Model\Repositories;

use Webkul\Core\Eloquent\Repository;
use Wewebit\Model\Models\Branch;

class BranchRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return Branch::class;
    }
}