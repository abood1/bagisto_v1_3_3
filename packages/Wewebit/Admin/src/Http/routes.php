<?php
use Wewebit\Admin\Http\Controllers\BranchController;

Route::group([
        'prefix'        => 'admin/catalog/branches',
        'middleware'    => ['web', 'admin'],
        'namespace'     => 'Wewebit\Admin\Http\Controllers'
    ], function () {
    Route::get('create',[BranchController::class,'create']);
    Route::post('create', 'Webit\Wholesale\Http\Controllers\Admin\AdminSectorController@store');
});