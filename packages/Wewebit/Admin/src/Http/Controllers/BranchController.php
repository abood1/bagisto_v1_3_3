<?php

namespace Wewebit\Admin\Http\Controllers;

use Wewebit\Model\Repositories\BranchRepository;
use Illuminate\Http\Request;

/**
 * Description of BranchController
 *
 * @author abdullah
 */
class BranchController {
    
    
    /**
     * Repository object
     *
     * @var BranchRepository
     */
    private $branchRepository;
    
    
    /**
     * Create a new controller instance.
     *
     * @param  BranchRepository  $branchRepository
     * @return void
     */
    public function __construct(
        BranchRepository $branchRepository
    )   {

        $this->branchRepository = $branchRepository;
    }


    public function create() {
        return view('webit.admin::branch.create');
    }
    
    public function store(Request $request) {
        $this->branchRepository->create($request->post());
    }
}
