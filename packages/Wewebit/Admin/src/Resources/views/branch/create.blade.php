@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.marketing.campaigns.add-title') }}
@stop

@section('content')

<input type="hidden" name="lat" />
<input type="hidden" name="lng" />
<div class="control-group">
    <label>Name:</label>
    <input type="text" class="control" name="name" />
</div>
<div class="control-group">
    <label>Description:</label>
    <textarea class="control" name="description"></textarea>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" name="main_branch" />
  <label class="form-check-label" for="flexCheckIndeterminate">
    Is Main Branch
  </label>
</div>

<h4>Choose Location</h4>
<map-component></map-component>




@stop

@push('scripts')
<script src='https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.js'></script>
<link href='https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.css' rel='stylesheet' />
<script
    type="text/javascript"
    baseUrl="{{ url()->to('/') }}"
    src="{{ asset('themes/velocity/assets/js/webit-ui.js') }}">
</script>
@endpush