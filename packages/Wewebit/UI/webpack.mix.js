const mix = require("laravel-mix");

if (mix == 'undefined') {
    const { mix } = require("laravel-mix");
}

require('laravel-mix-merge-manifest');

let publicPath = '../../../public/themes/velocity/assets';

if (mix.inProduction()) {
    publicPath = 'publishable/assets';
}

mix.setPublicPath(publicPath).mergeManifest();
mix.disableNotifications();

mix
    .js(
        __dirname + '/src/Resources/assets/js/app.js',
        'js/webit-ui.js'
    ).vue()
    .options({
        processCssUrls: false
    });

if (mix.inProduction()) {
    mix.version();
}