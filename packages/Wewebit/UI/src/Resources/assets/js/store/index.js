
import moduleMap from "./modules/map";
//import { createStore } from 'vuex'

import Vuex from 'vuex'
//

//Vue.use(Vuex)

//Vue.use(Vuex); // using Vuex

export default new Vuex.Store({
        
  modules: {
    moduleMap: moduleMap
  }
});
