export default {
    
  state: () => ({
    coordinates: []
  }),
  mutations: {
    markerSelected (state,payload) {
        state.coordinates.latitude = payload[0]
        state.coordinates.longitude = payload[1]
        $('input[name="lat"]').val(payload[0])
        $('input[name="lng"]').val(payload[1])
    }
  }
}

