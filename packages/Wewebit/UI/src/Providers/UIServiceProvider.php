<?php

namespace Wewebit\UI\Providers;

use Illuminate\Support\ServiceProvider;

class UIServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../publishable/assets' => public_path('themes/velocity/assets'),
        ], 'public');

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }
}
