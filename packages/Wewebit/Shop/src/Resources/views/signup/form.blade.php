
           
<input type="hidden" name="lat" />
<input type="hidden" name="lng" />
            
         
                <div class="control-group">
                    <label>
                        Registration document
                    </label>
                    <input type="file" name="registration_document" class="form-control-file" />
                    @if ($errors->has('registration_document'))
                        <p class="">
                            {{ $errors->first('registration_document') }}
                        </p>
                    @endif
                </div>
                <div class="control-group">
                    <label>
                        Profession document
                    </label>
                    <input type="file" name="profession_document" class="form-control-file"  />
                    @if ($errors->has('profession_document'))
                        <p class="">
                            {{ $errors->first('profession_document') }}
                        </p>
                    @endif
                </div>

<h4>Choose Location</h4>
<map-component></map-component>


@push('scripts')
<script src='https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.js'></script>
<link href='https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.css' rel='stylesheet' />
<script
    type="text/javascript"
    baseUrl="{{ url()->to('/') }}"
    src="{{ asset('themes/velocity/assets/js/webit-ui.js') }}">
</script>
@endpush