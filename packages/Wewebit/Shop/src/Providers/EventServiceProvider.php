<?php

namespace Wewebit\Shop\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;
class EventServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
          
        Event::listen('bagisto.shop.customers.signup_form_controls.after', function($viewRenderEventManager) {
            $viewRenderEventManager->addTemplate('webit.shop::signup.form');
        });
    }
}