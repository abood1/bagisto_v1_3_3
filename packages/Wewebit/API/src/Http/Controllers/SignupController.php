<?php namespace Wewebit\API\Http\Controllers;


use Webkul\Customer\Repositories\CustomerRepository;
use Webkul\Customer\Repositories\CustomerGroupRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
/**
 * Description of SignupController
 *
 * @author abdullah
 */
class SignupController {
    
    
    /**
     * Repository object
     *
     * @var CustomerRepository
     */
    protected $customerRepository;
    
    /**
     * Repository object
     *
     * @var CustomerGroupRepository
     */
    protected $customerGroupRepository;
    
    
     /**
     * Create a new controller instance.
     *
     * @param  CustomerRepository  $customerRepository
     * @param  CustomerGroupRepository  $customerGroupRepository
     * @return void
     */
    public function __construct(
        CustomerRepository $customerRepository,
        CustomerGroupRepository $customerGroupRepository    
    )   {

        $this->customerRepository = $customerRepository;
        $this->customerGroupRepository = $customerGroupRepository;

    }
    
    /**
     * Method to store user's sign up form data to DB.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'email|required|unique:customers,email',
            'password'   => 'confirmed|min:6|required',
            'registration_document' => 'nullable|mimes:jpg,bmp,png,application/pdf,application/x-pdf,application/acrobat, applications/vnd.pdf',
            'profession_document' => 'nullable|mimes:jpg,bmp,png,application/pdf,application/x-pdf,application/acrobat, applications/vnd.pdf',
        ]);
        if ($validator->fails()){
            $errors = $validator->errors();
            return response()->json($errors->all(),500);
        }
        $data = [
            'first_name'  => $request->get('first_name'),
            'last_name'   => $request->get('last_name'),
            'email'       => $request->get('email'),
            'password'    => $request->get('password'),
            'password'    => bcrypt($request->get('password')),
            'channel_id'  => core()->getCurrentChannel()->id,
            'is_verified' => 1,
            'customer_group_id' => $this->customerGroupRepository->findOneWhere(['code' => 'general'])->id
        ];
        
        if ($request->hasFile('registration_document')) {
            $uploadedFile = $request->file('registration_document');
            $filename = time() . $uploadedFile->getClientOriginalName();
            $data['registration_document'] = $filename;
            Storage::disk('local')->putFileAs(
                    'public/files/', $uploadedFile, $filename
            );
        }
        if ($request->hasFile('profession_document')) {
            $uploadedFile = $request->file('profession_document');
            $filename = time() . $uploadedFile->getClientOriginalName();
            $data['profession_document'] = $filename;
            Storage::disk('local')->putFileAs(
                    'public/files/', $uploadedFile, $filename
            );
        }


        $customer = $this->customerRepository->create($data);

        return response()->json([
            'message' => 'Your account has been created successfully.',
        ]);
    }
}
