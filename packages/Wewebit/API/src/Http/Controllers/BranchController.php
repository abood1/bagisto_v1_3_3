<?php

namespace Wewebit\API\Http\Controllers;


use Wewebit\Model\Repositories\BranchRepository;
use Illuminate\Http\Request;

/**
 * Description of BranchController
 *
 * @author abdullah
 */
class BranchController {
    
    
    /**
     * Repository object
     *
     * @var BranchRepository
     */
    private $branchRepository;
    
    
    /**
     * Create a new controller instance.
     *
     * @param  BranchRepository  $branchRepository
     * @return void
     */
    public function __construct(
        BranchRepository $branchRepository
    )   {

        $this->branchRepository = $branchRepository;
    }
    
    public function store(Request $request) {
        $request->validate([
            'lat' => 'required',
            'lag' => 'required',
            'name' => 'required',
            'description' => 'nullable'
        ]);
        $this->branchRepository->create($request->post());
    }
}
