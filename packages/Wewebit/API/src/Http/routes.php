<?php
use Wewebit\API\Http\Controllers\{SignupController,BranchController};

Route::group([
        'prefix'        => 'api/v2',
        'middleware'    => ['locale', 'theme', 'currency'],
        'namespace'     => 'Wewebit\API\Http\Controllers'
    ], function () {
    Route::post('customer/register',[SignupController::class,'create']);
});

Route::group([
        'prefix'        => 'api',
        'middleware'    => ['locale', 'theme', 'currency'],
        'namespace'     => 'Wewebit\API\Http\Controllers'
    ], function () {
    Route::post('branches',[BranchController::class,'store']);
});